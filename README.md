# Dictionnaire Français-Morwuwhw

Une application web de type dictionnaire linguistique, pour gérer une liste de mots de vocabulaire. Il s’agit d’un dictionnaire morwuwhw-français, où le morwuwhw est le nom d’une langue que j’ai inventé.

# Licence

Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0).
Voir le fichier LICENSE.
