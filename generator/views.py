from django.shortcuts import render
from dictionnary.models import Word
from generator.forms import GeneratorForm
from generator import matrix as mx
from generator import generator as gen

def generator_form_view(request):
    form = GeneratorForm()

    if request.method == 'POST':
        form = GeneratorForm(request.POST)

        if form.is_valid():
            examples_words = Word.objects.values_list('morwuwhw')
            words = []
            for word in examples_words:
                words.append(word[0])
            matrix = mx.generate_matrix(words)
            gen_words = gen.generate_words(
                max_mots=form.cleaned_data['max_words'],
                word_size=form.cleaned_data['size_words'],
                p=matrix,
                in_database_words=words
            )
            return render(request,'generator/generator_form.html',{'gen_words':gen_words})
        
    return render(request,'generator/generator_form.html',{'form':form})
