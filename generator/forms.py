from django import forms

class GeneratorForm(forms.Form):
    max_words = forms.IntegerField(min_value=1,max_value=100)
    size_words = forms.IntegerField(min_value=1,max_value=40)
