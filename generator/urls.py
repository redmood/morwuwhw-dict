from django.urls import path
from generator import views

app_name = 'generator'

urlpatterns = [
    path('', views.generator_form_view, name='setgenerator'),
]
