from django.db import models

class Character(models.Model):
    SORROR='Sorror'
    MATER='Mater'
    PLANET_CHOICES = [
        (SORROR, 'Sorror'),
        (MATER, 'Mater'),
    ]

    MALE='male cis'
    FEMALE='female cis'
    TRANSMALE='male trans'
    TRANSFEMALE='female trans'
    NB='non binary'
    AGENDER='agender'
    GENDERFLUID='genderfluid'
    ELSE='whatever they want be'

    GENDER_CHOICES= [
        (MALE, 'male cis'),
        (FEMALE, 'female cis'),
        (TRANSMALE, 'male trans'),
        (TRANSFEMALE, 'female trans'),
        (NB, 'non binary'),
        (AGENDER, 'agender'),
        (GENDERFLUID, 'genderfluid'),
        (ELSE, 'whatever they want be'),
    ]

    HETEROSEXUAL='hetero'
    HOMOSEXUAL='homo'
    BISEXUAL='bi'
    PANSEXUAL='pan'
    ANDROSEXUAL='andro'
    GYNESEXUAL='gyne'
    ASEXUAL='ace'
    BICURIOUS='bicurious'

    SEXUAL_ORIENTATIONS= [
        (HETEROSEXUAL, 'hetero'),
        (HOMOSEXUAL, 'homo'),
        (BISEXUAL, 'bi'),
        (PANSEXUAL, 'pan'),
        (ANDROSEXUAL, 'andro'),
        (GYNESEXUAL, 'gyne'),
        (ASEXUAL, 'ace'),
        (BICURIOUS, 'bicurious'),
    ]

    firstname=models.CharField(max_length=25,null=False,blank=True)
    middlename=models.CharField(max_length=25,null=False,blank=True)
    lastname=models.CharField(max_length=50,null=False,blank=True)
    birth_date=models.DateTimeField(auto_now=False,null=True)
    birth_planet=models.CharField(
        max_length=6,
        choices=PLANET_CHOICES,
        blank=True,
    )
    birth_place=models.CharField(max_length=50,null=False,blank=True)

    weight=models.FloatField(null=True)
    height=models.IntegerField(null=True)
    gender=models.CharField(
        max_length=22,
        choices=GENDER_CHOICES,
        default=ELSE,
    )

    BON='bon'
    NEUTRE='neutre'
    MAUVAIS='mauvais'
    LOYAL='loyal'
    CHAOTIQUE='chaotique'

    H_ALIGNEMENTS= [
        (BON, 'bon'),
        (NEUTRE, 'neutre'),
        (MAUVAIS, 'mauvais'),
    ]

    V_ALIGNEMENTS= [
        (LOYAL, 'loyal'),
        (NEUTRE, 'neutre'),
        (CHAOTIQUE, 'chaotique'),
    ]

    foot_size=models.IntegerField(null=True)
    eye_color=models.CharField(max_length=25,null=False,blank=True)
    hair_color=models.CharField(max_length=25,null=False,blank=True)
    pilosity=models.TextField(null=False,blank=True)
    body_precisions=models.TextField(null=False,blank=True)

    h_alignement=models.CharField(
        max_length=10,
        choices=H_ALIGNEMENTS,
        blank=True,
    )
    v_alignement=models.CharField(
        max_length=10,
        choices=V_ALIGNEMENTS,
        blank=True,
    )
    evolution=models.TextField(null=False,blank=True)
    former_life=models.TextField(null=False,blank=True)
    history_arc=models.TextField(null=False,blank=True)
    afetr_history=models.TextField(null=False,blank=True)

    main_values=models.TextField(null=False,blank=True)
    main_qualities=models.TextField(null=False,blank=True)
    main_flaws=models.TextField(null=False,blank=True)

    favorite_color=models.CharField(max_length=25,null=False,blank=True)
    favorite_meal=models.CharField(max_length=25,null=False,blank=True)
    animals=models.TextField(null=False,blank=True)
    hobbies=models.TextField(null=False,blank=True)
    sexual_orientation=models.CharField(
        max_length=10,
        choices=SEXUAL_ORIENTATIONS,
        blank=True,
    )

    secret_gifts=models.TextField(null=False,blank=True)
    hidden_garden=models.TextField(null=False,blank=True)

    relationships=models.ManyToManyField(
        Character,
        through='Relationship',
        through_fields=('character1','character2'),
    )

class Relationship(models.Model):
    character1=models.ForeignKey(Character,on_delete=models.CASCADE)
    character2=models.ForeignKey(Character,on_delete=models.CASCADE)
    relation=models.TextField(null=False,blank=True)
