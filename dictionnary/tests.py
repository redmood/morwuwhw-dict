import csv
from django.test import TestCase, Client
from django.utils import timezone
from dictionnary.models import Word
from dictionnary.views import search_word
from dictionnary.forms import SearchForm

class TestBase(TestCase):

    @classmethod
    def readDataFromFile(self):
        dataReader = csv.reader(open('tests_data/data.csv',newline='\n'),delimiter=';',quotechar='"')
        for row in dataReader:
            Word.objects.create(french=row[0],morwuwhw=row[1],precisions=row[2],entry_date=timezone.now())

class TestWord(TestBase):

    def setUp(self):
        TestBase.readDataFromFile()

    def testStr(self):
        avoir = Word.objects.get(french='avoir')
        voir = Word.objects.get(french='voir')

        self.assertEqual(avoir.__str__(), 'avoir : hari (verbe avoi)')
        self.assertEqual(voir.__str__(), 'voir : vaio (verbe voir)')
