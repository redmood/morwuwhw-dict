from django.urls import path
from dictionnary import views

app_name = 'dictionnary'

urlpatterns = [
    path('createword/', views.WordCreateView.as_view(), name='createword'),
    path('listword/', views.WordListView.as_view(), name='listword'),
    path('updateword/<int:pk>', views.WordUpdateView.as_view(), name='updateword'),
    path('detailword/<int:pk>', views.WordDetailView.as_view(), name='detailword'),
    path('searchword/',views.search_word,name='searchword'),
	path('deleteword/<int:pk>',views.WordDeleteView.as_view(),name='deleteword'),
]
