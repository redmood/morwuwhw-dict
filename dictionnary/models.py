from django.db import models
from django.urls import reverse
from django.utils.timezone import now

class Word(models.Model):
    morwuwhw = models.CharField(null=False,blank=False,max_length=50)
    french = models.CharField(null=False,blank=False,max_length=50)
    precisions = models.TextField(null=False,blank=True)
    entry_date = models.DateField(default=now)

    def __str__(self):
        return self.french + " : " + self.morwuwhw + " (" + self.precisions[0:10] + ")"

    def get_absolute_url(self):
        return reverse("dictionnary:createword",kwargs={})
